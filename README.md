![Logo](./figs/RATLogo.png)
# RAT-CCT
<em>Hmm CCT you say, interesting.</em>

## Introduction
This is a wrapper around rat to generate simple CCT geometries using a straight forward Json input file. This tool is intended for non-programmers to play with CCT magnets and is limited in the sense that it is not possible to model a combined function magnet with it. 

## Installation
This tool depends on the full set of Rat libraries (see documentation: [rat-docs](https://gitlab.com/Project-Rat/rat-documentation)), which are required to build this CCT tool. After the Rat libraries have been build and installed the compilation is achieved with:
```
mkdir release
cd release
cmake -DCMAKE_BUILD_TYPE=T ..
make -jX
```
After compiling the code can be called from the release directory as
```
./models/cct -f ../json/cct.json
```
How to create docker file?

## Gallery
MCBX-CCT Alternative.
![MCBX3D](./figs/MCBXShort.png)

MCBX-CCT Alternative.
![MCBX2D](./figs/MCBXCross.png)

## Contribution
Not yet.

## Versioning
We're still in alpha.

## Authors
* Jeroen van Nugteren

## License
This project is licensed under the GNU general public license.