/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// general header files
#include <armadillo>
#include <iostream>
#include <cmath>
#include <complex>
#include <cassert>

// header files for TCLAP
#include <tclap/CmdLine.h>

// header files from Common
#include "rat/common/serializer.hh"
#include "rat/common/log.hh"
#include "rat/common/opera.hh"
#include "rat/common/freecad.hh"
#include "rat/common/gmshfile.hh"

// material header files
#include "rat/mat/parallelconductor.hh"
#include "rat/mat/rutherfordcable.hh"
#include "rat/mat/ltswire.hh"

// header files for Models
#include "rat/models/crossrectangle.hh"
#include "rat/models/modelcoil.hh"
#include "rat/models/pathcct.hh"
#include "rat/models/modelgroup.hh"
#include "rat/models/calcline.hh"
#include "rat/models/calcharmonics.hh"
#include "rat/models/driveac.hh"
#include "rat/models/emitterbeam.hh"
#include "rat/models/crosscircle.hh"
#include "rat/models/pathaxis.hh"
#include "rat/models/transbend.hh"
#include "rat/models/calcgrid.hh"
#include "rat/models/calcmesh.hh"
#include "rat/models/modelinductance.hh"
#include "rat/models/calcsurface.hh"
#include "rat/models/background.hh"
#include "rat/models/calclength.hh"
#include "rat/models/serializer.hh"
#include "rat/models/modelmlfmm.hh"


// main
int main(int argc, char * argv[]){
  try 
  {
// *** TClap command line *** //
    TCLAP::CmdLine cmd("Simple CCT Magnet program",' ', "v08/11/2020");

    // Calculation options
    TCLAP::SwitchArg surface_argument("s","surface","Calculate the field on the surface of the coil.", cmd, false);
    TCLAP::SwitchArg line_argument("r","r_line","Calculate the field on a line through the coil.", cmd, false);    
    TCLAP::SwitchArg harmonics_argument("q","quality","Calculate the harmonics for field quality.", cmd, false);
    TCLAP::SwitchArg inductance_argument("l","inductance","Calculate the inductance matrix.", cmd, false);
    TCLAP::SwitchArg grid_argument("g","grid","Calculate the field surrounding the coil.", cmd, false);
    TCLAP::SwitchArg freecad_argument("c","cad","Export a freecad file.", cmd, false);
    TCLAP::SwitchArg opera_argument("p","opera","Export to opera file.", cmd, false);
    TCLAP::SwitchArg json_argument("j","json","Export to json file.", cmd, false);
    TCLAP::SwitchArg curvature_argument("C","compensate_curvature","Compensate for curvature.", cmd, false);
    TCLAP::SwitchArg conductor_length_argument("L","conductor_length","Calculate the length of the conductor.", cmd, false);

    // Input and output files and dirs
    TCLAP::ValueArg<std::string> input_argument("i", "input_file", "Name of the input json file.", false, "./json/models/simplecct.json", "string", cmd);
    TCLAP::ValueArg<std::string> output_argument("o", "output_file", "Name of the output paraview file.", false, "cct", "string", cmd);
    TCLAP::ValueArg<std::string> output_dir_argument("d", "output_dir", "Name of the output directory.", false, "./simplecct/", "string", cmd);

    // Parse the command line arguments, and retrieve the values
    cmd.parse(argc, argv);
    bool calc_surface                   = surface_argument.getValue();
    bool calc_line						= line_argument.getValue();
    bool calc_harmonics                 = harmonics_argument.getValue();
    bool calc_grid		                = grid_argument.getValue();
    bool calc_inductance                = inductance_argument.getValue();
    bool compensate_curvature 			= curvature_argument.getValue();

    std::string ifname		            = input_argument.getValue();
    std::string fname		            = output_argument.getValue();
    std::string dir			            = output_dir_argument.getValue();
    bool calc_opera						= opera_argument.getValue();
    bool calc_json						= json_argument.getValue();
    bool calc_freecad					= freecad_argument.getValue();
    bool calc_length					= conductor_length_argument.getValue();

	// get path
	boost::filesystem::path ifpath(ifname);

	// create serialization object
	rat::cmn::ShSerializerPr slzr = rat::cmn::Serializer::create();

	// create json tree
	Json::Value root;

	// create reader
	Json::CharReaderBuilder rbuilder;
	
	// open file for reading
	std::ifstream if_id(ifpath.string(), std::ifstream::in);
	if(!if_id.good())rat_throw_line("input file doesn't exist");

	// write
	std::string errs;
	bool ok = Json::parseFromStream(rbuilder, if_id, &root, &errs);

	// close input file
	if_id.close();
	
	// check for errors
	if(ok==false)rat_throw_line("json parsing error: " + errs);

	// data storage directory and filename
	const std::string name = root["name"].asString();
	
	// overall parameters
	const double rbend = root["rbend"].asDouble();

	// number of coils
	const arma::uword num_coils = root["coils"].size();

	// create bending transformation
	rat::mdl::ShTransBendPr bending = rat::mdl::TransBend::create(0,1,0, 0,0,1, rbend);

	// create model that combines coils
	rat::mdl::ShModelGroupPr model = rat::mdl::ModelGroup::create();

	// set coil length
	double magnet_ell = 0;
	double magnet_rad = 0;
	
	// walk over coils
	for(arma::uword i=0;i<num_coils;i++){
		// get json
		Json::Value js = root["coils"].get(i,0);

		// conductor parameters
		const arma::uword nd = js["nd"].asUInt64(); // number of wires in width of slot
		const arma::uword nw = js["nw"].asUInt64(); // number of wires in depth of slot
		const double dcable = js["dcable"].asDouble();
		const double wcable = js["wcable"].asDouble();

		// coil parameters
		const std::string coil_name = js["name"].asString();
		const bool reverse = js["reverse"].asBool();
		const bool horizontal = js["horizontal"].asBool();
		const arma::uword n = js["n"].asUInt64();
		const double rref = js["rref"].asDouble(); // reference radius
		const double rcoil = js["rcoil"].asDouble(); // coil radius
		const double delta = js["delta"].asDouble(); // spacing between turns
		const double alpha = arma::datum::pi*2*js["alpha"].asDouble()/360; 
		const double ell = js["ell"].asDouble();
		const arma::uword nnpt = js["nnpt"].asUInt64();
		
		// operating conditions
		const double current = js["current"].asDouble();
		const double temperature = js["temperature"].asDouble();
		//const arma::uword drive_id = js["drive_id"].asUInt64();

		// update coil length
		magnet_ell = std::max(magnet_ell, ell);
		magnet_rad = std::max(magnet_rad, rcoil+wcable);

		// material creating coil to coil connections (e.g. indium)
		rat::mat::ShLTSWirePr nbti_wire;
		boost::filesystem::path filepath = js["wire"].asString();
		if(!filepath.is_absolute())filepath = ifpath.parent_path()/filepath;
		nbti_wire = rat::mdl::Serializer::create<rat::mat::LTSWire>(filepath);
	
		// construct cable based on number of strands and channel size
		rat::mat::ShRutherfordCablePr nbti_cable = 
			rat::mat::RutherfordCable::create();;
		nbti_cable->set_strand(nbti_wire);
		nbti_cable->set_num_strands(nd*nw);
		nbti_cable->set_width(wcable);
		nbti_cable->set_thickness(dcable);
		nbti_cable->set_keystone(0);
		nbti_cable->set_pitch(0);
		nbti_cable->set_dinsu(0);
		nbti_cable->set_fcabling(1.0);

		// inner radius parameters
		const double omega = (delta + dcable)/std::sin(alpha);
		const double aest = rref/(n*std::tan(alpha)); // skew amplitude
		const double nt = 2*std::floor((ell-2*aest)/(2*omega));

		// account for integer number of turns
		double a = n*(ell - nt*omega)/2;
		if(!reverse)a+=omega/(n*4); else a+=omega*(n*4-1)/(n*4);

		// create path
		rat::mdl::ShPathCCTPr path_cct = rat::mdl::PathCCT::create(n,rcoil,a,omega,nt,nnpt);
		
		// slanting direction
		path_cct->set_is_reverse(!reverse);

		// horizontal
		if(horizontal)path_cct->add_rotation(0,0,1,arma::datum::pi/2);

		// add bending
		path_cct->add_transformation(bending);

		// create cable cross section
		// note that the cable is centered at least
		// in the thickness direction
		rat::mdl::ShCrossRectanglePr cross_rect = 
			rat::mdl::CrossRectangle::create(-dcable/2,dcable/2,0,wcable,nd,nw);

		// create inner and outer coils
		rat::mdl::ShModelCoilPr coil = rat::mdl::ModelCoil::create(
			path_cct, cross_rect, nbti_cable);
		coil->set_name(coil_name); 
		coil->set_number_turns(nd*nw);
		coil->set_operating_current(current);
		coil->set_operating_temperature(temperature);
		//coil->set_drive_id(drive_id);

		// add coil to model
		model->add_model(coil);
	}

	// axis of magnet for field quality calculation
	rat::mdl::ShPathAxisPr pth = rat::mdl::PathAxis::create('z','y',magnet_ell+0.4,0,0,0,2e-3);
	pth->add_transformation(bending);


	// CALCULATION AND OUTPUT
	// create log
	rat::cmn::ShLogPr lg = rat::cmn::Log::create();

    // Create a list of meshes
    rat::mdl::ShModelMlfmmPr mlfmm = rat::mdl::ModelMlfmm::create(model);
    mlfmm->set_output_dir(dir);
    mlfmm->set_target_meshes(); 

	// calculate field on the surface of the coil
	mlfmm->set_target_surface(calc_surface);

	// calculate field on line
	if(calc_line){
		// calculate field
		rat::mdl::ShCalcLinePr line = rat::mdl::CalcLine::create(pth);
		mlfmm->add_target(line);
	}

	// grid calculation
	if(calc_grid){
		mlfmm->set_target_grid();
	}

	// calculate inductance
	if(calc_inductance){
		rat::mdl::ShModelInductancePr ind = rat::mdl::ModelInductance::create(model);
		ind->calculate(lg); 
		ind->display(lg);
	}

	// simple line calculation
	if(calc_length){
		rat::mdl::ShCalcLengthPr length = rat::mdl::CalcLength::create(model->create_meshes());
	}	

	// calculate harmonics
	if(calc_harmonics){	
		// create calculation
		rat::mdl::ShModelMlfmmPr mlfmm_exp8 = rat::mdl::ModelMlfmm::create(model);
		mlfmm_exp8->set_output_dir(dir);

		// set number of expansions used for the spherical harmonics
		// higher is more accurate but also slower. The default is 5
		// but for harmonics we're using 8 here.
		mlfmm_exp8->get_settings()->set_num_exp(8);

		// create harmonic coil calculation along axis 
		// second input is the reference radius
		const rat::fltp reference_radius = 10e-3;
		rat::mdl::ShCalcHarmonicsPr harm = rat::mdl::CalcHarmonics::create(pth, reference_radius);
		harm->set_compensate_curvature(compensate_curvature);

		// number of points used for each 
		// harmonic calculation circle
		harm->set_num_theta(64);

		// add harmonics calculation as a target
		mlfmm_exp8->add_target(harm);

		// run calculation
		mlfmm_exp8->calculate(lg);

		// output to terminal
		harm->display(lg);
	}

	// // write a freecad macro
	if(calc_freecad){
		model->export_freecad(rat::cmn::FreeCAD::create(dir + fname + ".FCMacro", name));
	}

	// export to an opera conductor file
	if(calc_opera){
		model->export_opera(rat::cmn::Opera::create(dir + fname + ".cond"));
	}

	// export json
	if(calc_json){
		// write to output file
		rat::cmn::ShSerializerPr slzr = rat::cmn::Serializer::create();
		slzr->flatten_tree(model); slzr->export_json(dir + fname + ".json");
	}
    mlfmm->calculate(lg);

  } // end try 
  catch (TCLAP::ArgException &e)  // catch any exceptions
  { 
    std::cerr << "error: " << e.error() << " for arg " << e.argId() << std::endl; 
  } // end catch
}