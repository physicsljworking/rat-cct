/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// general header files
#include <armadillo>
#include <iostream>
#include <cmath>
#include <complex>
#include <cassert>

// header files for common
#include "rat/common/log.hh"

// header files for MLFMM
#include "rat/mlfmm/mlfmm.hh"

// material header files
#include "rat/mat/parallelconductor.hh"
#include "rat/mat/rutherfordcable.hh"
#include "rat/mat/ltswire.hh"

// header files for Models
#include "rat/models/pathcctcustom.hh"
#include "rat/models/frame.hh"
#include "rat/models/crossrectangle.hh"
#include "rat/models/modelcoil.hh"
#include "rat/models/modelgroup.hh"
#include "rat/models/calcharmonics.hh"
#include "rat/models/transbend.hh"
#include "rat/models/calcgrid.hh"
#include "rat/models/calcmesh.hh"
#include "rat/models/serializer.hh"
#include "rat/models/modelmlfmm.hh"
#include "rat/models/modelinductance.hh"

// main
int main(){
	// INPUT SETTINGS
	// switchboard for calculation types
	const bool run_coil_field = true; // calculate field on the coil
	const bool run_surface_field = true; // calculate field on the surface of the coil
	const bool run_grid = true; // calculate field on a grid of points surrounding the coil
	const bool run_harmonics = true; // run harmonic coil calculation
	const bool run_inductance = true; // calculate inductance of coil

	// data storage
	const boost::filesystem::path output_dir = "./mixedcct/";

	// geometry settings
	const double nt = 80;
	const double radius1 = 0.025; // radius
	const double radius2 = 0.025 + 7e-3; // radius
	const double omega = 8e-3;
	const double operating_current = 480;
	const double operating_temperature = 4.5;
	const double dcable = 2e-3;
	const double wcable = 5e-3;
	const double element_size = 2e-3;
	const arma::uword nnpt = 240;
	const double dipole_amplitude = 70e-3;
	const double quad_amplitude = -15e-3;
	const arma::uword num_quad = 40;
	
	const arma::uword nd = 2;
	const arma::uword nw = 5;
	const double bending_radius = 0.7;



	// GEOMETRY SETUP
	// construct conductor
	rat::mat::ShLTSWirePr nbti_wire = 
		rat::mdl::Serializer::create<rat::mat::LTSWire>(
		"json/materials/ltswire/NbTi_LHC_1p9.json");
	rat::mat::ShRutherfordCablePr nbti_cable = 
		rat::mat::RutherfordCable::create();;
	nbti_cable->set_strand(nbti_wire);
	nbti_cable->set_num_strands(nd*nw);
	nbti_cable->set_width(wcable);
	nbti_cable->set_thickness(dcable);
	nbti_cable->set_keystone(0);
	nbti_cable->set_pitch(0);
	nbti_cable->set_dinsu(0);
	nbti_cable->set_fcabling(1.0);

	// create custom CCT path
	rat::mdl::ShPathCCTCustomPr path_cct1 = rat::mdl::PathCCTCustom::create();
	path_cct1->set_range(-nt/2,nt/2);
	path_cct1->set_geometry(
		arma::Row<double>{-nt/2,nt/2},
		arma::Row<double>{omega,omega},
		arma::Row<double>{radius1,radius1});
	path_cct1->set_num_nodes_per_turn(nnpt);

	// add dipole
	path_cct1->add_harmonic(rat::mdl::CCTHarmonicInterp::create(
		1,false,arma::Row<double>{-nt/2,nt/2},
		-arma::Row<double>{dipole_amplitude,dipole_amplitude}));

	// add quadrupole
	path_cct1->add_harmonic(rat::mdl::CCTHarmonicEquation::create(2,false,
		[&](const arma::Row<double> &turn){const arma::Row<double> alpha = 
			quad_amplitude*arma::cos(arma::datum::pi*2.0*turn/num_quad); return alpha;}));
	path_cct1->add_harmonic(rat::mdl::CCTHarmonicEquation::create(2,true,
		[&](const arma::Row<double> &turn){const arma::Row<double> alpha = 
			quad_amplitude*arma::sin(arma::datum::pi*2.0*turn/num_quad); return alpha;}));

	// transformations
	path_cct1->add_rotation(1,0,0,arma::datum::pi/2);

	// create custom CCT path
	rat::mdl::ShPathCCTCustomPr path_cct2 = rat::mdl::PathCCTCustom::create();
	path_cct2->set_range(-nt/2,nt/2);
	path_cct2->set_geometry(
		arma::Row<double>{-nt/2,nt/2},
		arma::Row<double>{omega,omega},
		arma::Row<double>{radius2,radius2});
	path_cct2->set_num_nodes_per_turn(nnpt);

	// add dipole
	path_cct2->add_harmonic(rat::mdl::CCTHarmonicInterp::create(
		1,false,arma::Row<double>{-nt/2,nt/2},
		arma::Row<double>{dipole_amplitude,dipole_amplitude}));

	// add quadrupole
	path_cct2->add_harmonic(rat::mdl::CCTHarmonicEquation::create(2,false,
		[&](const arma::Row<double> &turn){const arma::Row<double> alpha = 
			-quad_amplitude*arma::cos(arma::datum::pi*2.0*turn/num_quad); return alpha;}));
	path_cct2->add_harmonic(rat::mdl::CCTHarmonicEquation::create(2,true,
		[&](const arma::Row<double> &turn){const arma::Row<double> alpha = 
			-quad_amplitude*arma::sin(arma::datum::pi*2.0*turn/num_quad); return alpha;}));

	// transformations
	path_cct2->add_reverse();
	path_cct2->add_flip();
	path_cct2->add_rotation(1,0,0,arma::datum::pi/2);

	// axis of magnet for field quality calculation
	rat::mdl::ShPathGroupPr pth = rat::mdl::PathGroup::create();
	pth->add_path(rat::mdl::PathStraight::create(1.4,2e-3));
	pth->add_translation(0,-1.4/2,0);
	// pth->add_rotation(1,0,0,arma::datum::pi/2);
	// pth->add_rotation(0,0,1,arma::datum::pi);
	// pth->add_rotation(1,0,0,arma::datum::pi/2);

	// add bending
	if(bending_radius!=0){
		rat::mdl::ShTransBendPr bending = rat::mdl::TransBend::create(0,0,1, 0,1,0, bending_radius);
		path_cct1->add_transformation(bending);
		path_cct2->add_transformation(bending);
		pth->add_transformation(bending);
	}

	// create cross section
	rat::mdl::ShCrossRectanglePr cross_rect = rat::mdl::CrossRectangle::create(
		-dcable/2,dcable/2,0,wcable,element_size);

	// create coil
	rat::mdl::ShModelCoilPr coil1 = rat::mdl::ModelCoil::create(path_cct1, cross_rect, nbti_cable);
	coil1->set_name("inner");
	coil1->set_number_turns(nd*nw);
	coil1->set_operating_current(operating_current);
	coil1->set_operating_temperature(operating_temperature);
	
	rat::mdl::ShModelCoilPr coil2 = rat::mdl::ModelCoil::create(path_cct2, cross_rect, nbti_cable);
	coil2->set_name("outer");
	coil2->set_number_turns(nd*nw);
	coil2->set_operating_current(operating_current);
	coil2->set_operating_temperature(operating_temperature);
	
	// create model 
	rat::mdl::ShModelGroupPr model = rat::mdl::ModelGroup::create();
	model->add_model(coil1);
	model->add_model(coil2);


	// CALCULATION AND OUTPUT
	// create logger
	rat::cmn::ShLogPr lg = rat::cmn::Log::create(rat::cmn::Log::LogoType::RAT);

	// create calculation
	rat::mdl::ShModelMlfmmPr mlfmm = 
		rat::mdl::ModelMlfmm::create(model);
	mlfmm->set_output_dir(output_dir);
	mlfmm->set_target_meshes(run_coil_field); 
	mlfmm->set_target_grid(run_grid);
	mlfmm->set_target_surface(run_surface_field);

	// calculate everything
	mlfmm->calculate(lg);

	// inductance calculation
	if(run_inductance){
		// create inductance calculation
		rat::mdl::ShModelInductancePr ind = 
			rat::mdl::ModelInductance::create(model);

		// calculate and show matrix in terminal
		ind->calculate(lg); ind->display(lg);
	}

	// calculate harmonics
	if(run_harmonics){	
		// create calculation
		rat::mdl::ShModelMlfmmPr mlfmm_exp8 = rat::mdl::ModelMlfmm::create(model);
		mlfmm_exp8->set_output_dir(output_dir);

		// set number of expansions used for the spherical harmonics
		// higher is more accurate but also slower. The default is 5
		// but for harmonics we're using 8 here.
		mlfmm_exp8->get_settings()->set_num_exp(8);

		// create harmonic coil calculation along axis 
		// second input is the reference radius
		const rat::fltp reference_radius = 10e-3;
		rat::mdl::ShCalcHarmonicsPr harm = rat::mdl::CalcHarmonics::create(pth, reference_radius);
		harm->set_compensate_curvature(true);

		// number of points used for each 
		// harmonic calculation circle
		harm->set_num_theta(64);

		// add harmonics calculation as a target
		mlfmm_exp8->add_target(harm);

		// run calculation
		mlfmm_exp8->calculate(lg);

		// output to terminal
		harm->display(lg);
	}

}