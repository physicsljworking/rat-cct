/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// general header files
#include <armadillo>
#include <iostream>
#include <cmath>
#include <complex>
#include <cassert>

// common header files
#include "rat/common/log.hh"

// header files for mlfmm
#include "rat/mlfmm/mlfmm.hh"

// material header files
#include "rat/mat/parallelconductor.hh"
#include "rat/mat/rutherfordcable.hh"
#include "rat/mat/ltswire.hh"

// header files for Models
#include "rat/models/pathcctcustom.hh"
#include "rat/models/modelmesh.hh"
#include "rat/models/crossrectangle.hh"
#include "rat/models/modelcoil.hh"
#include "rat/models/modelgroup.hh"
#include "rat/models/calcharmonics.hh"
#include "rat/models/transbend.hh"
#include "rat/models/calcgrid.hh"
#include "rat/models/calctracks.hh"
#include "rat/models/emitterbeam.hh"
#include "rat/models/crosscircle.hh"
#include "rat/models/pathcircle.hh"
#include "rat/models/serializer.hh"
#include "rat/models/modelmlfmm.hh"


// main
int main(){
	// INPUT SETTINGS
	// calculation settings
	const bool run_coil_field = true;
	const bool run_surface_field = false;
	const bool run_harmonics = true;
	const bool run_grid = false;
	const bool run_tracking = true;

	// data storage
	const boost::filesystem::path output_dir = "./accelerator/";

	// cable settings
	const double dcable = 2e-3; // thickness of cable [m] (slot width)
	const double wcable = 5e-3; // width of cable [m] (slot depth)
	const double element_size = 2e-3; // size of the elements [m]
	const arma::uword num_strands = 10;	

	// geometry settings
	const double nt = 200; // number of turns
	const double radius = 0.025;
	const double dradial = 0.3e-3; // spacing between cylinders [m]
	const double dformer = 2e-3;
	const double r1 = radius+dformer; // inner coil radius [m]
	const double r2 = radius+2*dformer+wcable+dradial; // outer coil radius [m]
	const double omega = 6e-3; // winding pitch [m]
	const arma::uword nnpt = 240; // number of nodes per turn
	const double Rbend = nt*omega/(2*arma::datum::pi); // bend the magnet

	// strength of the different components
	const double dipole_amplitude = 50e-3; // strength of dipole [m]
	const double quad_amplitude = 8e-3; // strength of quadrupole [m]
	const double corr_quad_amplitude = 5.2e-3;

	// operating conditions
	const double operating_current = 480; // operating current [A]
	const double operating_temperature = 4.5; // operating temperature [K]

	// conductor settings
	const arma::uword nd = 2; // number of wires across cable thickness
	const arma::uword nw = 5; // number of wires across cable width
	const arma::uword num_quad = nt/4;


	// GEOMETRY SETUP
	// setup conductors
	// rat::mdl::ShMatGroupPr nbti_strand = rat::mdl::MatGroup::create();
	// {
	// 	// add nbti superconductor
	// 	rat::mdl::ShMatNbTiPr nbti = rat::mdl::MatNbTi::create();
	// 	nbti->set_nbti_LHC();
	// 	nbti_strand->add_material(ffill/(1.0+fcu2sc),nbti);

	// 	// add copper matrix
	// 	rat::mdl::ShMatCopperPr copper = rat::mdl::MatCopper::create();
	// 	copper->set_copper_OFHC_RRR100();
	// 	nbti_strand->add_material(ffill*fcu2sc/(1.0+fcu2sc),copper);
	// }

	// construct conductor
	rat::mat::ShLTSWirePr nbti_wire = 
		rat::mdl::Serializer::create<rat::mat::LTSWire>(
		"json/materials/ltswire/NbTi_LHC_1p9.json");
	rat::mat::ShRutherfordCablePr nbti_cable = 
		rat::mat::RutherfordCable::create();;
	nbti_cable->set_strand(nbti_wire);
	nbti_cable->set_num_strands(nd*nw);
	nbti_cable->set_width(wcable);
	nbti_cable->set_thickness(dcable);
	nbti_cable->set_keystone(0);
	nbti_cable->set_pitch(0);
	nbti_cable->set_dinsu(0);
	nbti_cable->set_fcabling(1.0);

	// create custom CCT path
	rat::mdl::ShPathCCTCustomPr path_cct1 = rat::mdl::PathCCTCustom::create();
	path_cct1->set_range(-nt/2,nt/2);
	path_cct1->set_geometry(
		arma::Row<double>{-nt/2,nt/2},
		arma::Row<double>{omega,omega},
		arma::Row<double>{r1,r1});
	path_cct1->set_num_nodes_per_turn(nnpt);

	// add dipole
	path_cct1->add_harmonic(rat::mdl::CCTHarmonicInterp::create(
		1,false,arma::Row<double>{-nt/2,nt/2},
		-arma::Row<double>{dipole_amplitude,dipole_amplitude}));

	// add quadrupole
	path_cct1->add_harmonic(rat::mdl::CCTHarmonicInterp::create(
		2,false,arma::Row<double>{-nt/2,nt/2},
		arma::Row<double>{corr_quad_amplitude,corr_quad_amplitude}));

	// add quadrupole
	path_cct1->add_harmonic(rat::mdl::CCTHarmonicEquation::create(2,false,
		[&](const arma::Row<double> &turn){const arma::Row<double> alpha = 
			quad_amplitude*arma::cos(arma::datum::pi*2.0*turn/num_quad); return alpha;}));
	path_cct1->add_harmonic(rat::mdl::CCTHarmonicEquation::create(2,true,
		[&](const arma::Row<double> &turn){const arma::Row<double> alpha = 
			quad_amplitude*arma::sin(arma::datum::pi*2.0*turn/num_quad); return alpha;}));

	// create custom CCT path
	rat::mdl::ShPathCCTCustomPr path_cct2 = rat::mdl::PathCCTCustom::create();
	path_cct2->set_range(-nt/2,nt/2);
	path_cct2->set_geometry(
		arma::Row<double>{-nt/2,nt/2},
		arma::Row<double>{omega,omega},
		arma::Row<double>{r2,r2});
	path_cct2->set_num_nodes_per_turn(nnpt);

	// add dipole
	path_cct2->add_harmonic(rat::mdl::CCTHarmonicInterp::create(
		1,false,arma::Row<double>{-nt/2,nt/2},
		arma::Row<double>{dipole_amplitude,dipole_amplitude}));

	// add quadrupole
	path_cct1->add_harmonic(rat::mdl::CCTHarmonicInterp::create(
		2,false,arma::Row<double>{-nt/2,nt/2},
		arma::Row<double>{corr_quad_amplitude,corr_quad_amplitude}));

	// add quadrupole
	path_cct2->add_harmonic(rat::mdl::CCTHarmonicEquation::create(2,false,
		[&](const arma::Row<double> &turn){const arma::Row<double> alpha = 
			-quad_amplitude*arma::cos(arma::datum::pi*2.0*turn/num_quad); return alpha;}));
	path_cct2->add_harmonic(rat::mdl::CCTHarmonicEquation::create(2,true,
		[&](const arma::Row<double> &turn){const arma::Row<double> alpha = 
			-quad_amplitude*arma::sin(arma::datum::pi*2.0*turn/num_quad); return alpha;}));

	// set reverse
	path_cct2->add_reverse();
	path_cct2->add_flip();

	// // axis of magnet for field quality calculation
	// ShPathGroupPr pth = PathGroup::create();
	// pth->add_path(PathStraight::create(0.5,1e-3));
	// pth->add_translation(0,-0.5/2,0);
	// pth->add_rotation(1,0,0,arma::datum::pi/2);
	// pth->add_rotation(0,0,1,arma::datum::pi);

	// add bending
	if(Rbend!=0){
		rat::mdl::ShTransBendPr bending = rat::mdl::TransBend::create(0,1,0, 0,0,1, Rbend);
		path_cct1->add_transformation(bending);
		path_cct2->add_transformation(bending);
		//pth->add_transformation(bending);
	}

	// create circle
	rat::mdl::ShPathCirclePr pth_axis = rat::mdl::PathCircle::create(Rbend,4,element_size);
	pth_axis->add_rotation(1,0,0,arma::datum::pi/2);
	pth_axis->add_translation(-Rbend,0,0);

	// create cross section
	rat::mdl::ShCrossRectanglePr cross_rect = rat::mdl::CrossRectangle::create(
		-dcable/2,dcable/2,0,wcable,element_size);

	// create coil
	rat::mdl::ShModelCoilPr coil1 = rat::mdl::ModelCoil::create(path_cct1, cross_rect);	
	coil1->set_name("inner");
	coil1->set_number_turns(num_strands);
	coil1->set_operating_temperature(operating_temperature);
	coil1->set_operating_current(operating_current);
	coil1->set_material(nbti_cable);

	// create second layer
	rat::mdl::ShModelCoilPr coil2 = rat::mdl::ModelCoil::create(path_cct2, cross_rect);	
	coil2->set_name("outer");
	coil2->set_number_turns(num_strands);
	coil2->set_operating_temperature(operating_temperature);
	coil2->set_operating_current(operating_current);
	coil2->set_material(nbti_cable);

	// create model 
	rat::mdl::ShModelGroupPr model = rat::mdl::ModelGroup::create();
	model->add_model(coil1); model->add_model(coil2);



	// CALCULATION AND OUTPUT
	// Create log
	// the default log displays the status in the
	// terminal window in which the code is called
	rat::cmn::ShLogPr lg = rat::cmn::Log::create(rat::cmn::Log::LogoType::RAT);

	// create calculation
	rat::mdl::ShModelMlfmmPr mlfmm = 
		rat::mdl::ModelMlfmm::create(model);
	mlfmm->set_output_dir(output_dir);
	mlfmm->set_target_meshes(run_coil_field); 
	mlfmm->set_target_surface(run_surface_field);
	mlfmm->set_target_grid(run_grid);

	// particle tracking
	rat::mdl::ShCalcMeshPr tracking_mesh;
	if(run_tracking){
		// set time
		const rat::fltp track_time = 0;

		// Create a grid of 140x160x50 points in the volume defined by intervals:
		rat::mdl::ShModelMeshPr beam_pipe = rat::mdl::ModelMesh::create(
			pth_axis, rat::mdl::CrossCircle::create(radius,element_size));

		// create mesh
		rat::mdl::ShCalcMeshPrList meshes = beam_pipe->create_meshes(track_time); tracking_mesh = meshes(0);
		tracking_mesh->set_field_type('H',3);

		// add the tracking mesh
		mlfmm->add_target(tracking_mesh);
	}

	// calculate everything
	mlfmm->calculate(lg);

	// run tracking code
	if(run_tracking){
		// check
		if(!tracking_mesh->has_field())rat_throw_line("mlfmm was not run on tracking mesh");
		if(!tracking_mesh->has('H'))rat_throw_line("magnetic field not calculated");

		// create an emitter
		rat::mdl::ShEmitterBeamPr beam = rat::mdl::EmitterBeam::create();
		beam->set_proton();
		beam->set_beam_energy(beam->get_rest_mass() + 0.00405); // GeV
		beam->set_spawn_coord(
			arma::Col<double>::fixed<3>{0,0,0},
			arma::Col<double>::fixed<3>{0,0,1},
			arma::Col<double>::fixed<3>{1,0,0},
			arma::Col<double>::fixed<3>{0,1,0});
		beam->set_xx(0.9, 0.005, 0.0*2*arma::datum::pi/360);
		beam->set_yy(0.9, 0.005, 0.0*2*arma::datum::pi/360);
		beam->set_start_idx(0);

		// create tracker
		rat::mdl::ShCalcTracksPr tracker = rat::mdl::CalcTracks::create(tracking_mesh, beam);
		tracker->set_stepsize(10e-3);

		// perform tracking algorithm
		tracker->create_tracks(lg);

		// write to vtk
		tracker->export_vtk()->write(output_dir/"tracks.vtu");
		tracking_mesh->export_vtk()->write(output_dir/"tracking_mesh.vtu");
	}

	// calculate harmonics
	if(run_harmonics){	
		// create calculation
		rat::mdl::ShModelMlfmmPr mlfmm_exp8 = rat::mdl::ModelMlfmm::create(model);

		// set number of expansions used for the spherical harmonics
		// higher is more accurate but also slower. The default is 5
		// but for harmonics we're using 8 here.
		mlfmm_exp8->get_settings()->set_num_exp(8);

		// create harmonic coil calculation along axis 
		// second input is the reference radius
		const rat::fltp reference_radius = 10e-3;
		rat::mdl::ShCalcHarmonicsPr harm = rat::mdl::CalcHarmonics::create(pth_axis, reference_radius);
		harm->set_compensate_curvature(true);

		// number of points used for each 
		// harmonic calculation circle
		harm->set_num_theta(64);

		// add harmonics calculation as a target
		mlfmm_exp8->add_target(harm);

		// run calculation
		mlfmm_exp8->calculate(lg);

		// export table
		harm->export_vtk_table()->write(output_dir/"harmonics.vtt");
	}









	// // CALCULATION AND OUTPUT
	// // Create log
	// // the default log displays the status in the
	// // terminal window in which the code is called
	// rat::cmn::ShLogPr lg = rat::cmn::Log::create();
 //    // Create a list of meshes
 //    rat::mdl::ShModelMlfmmPr mlfmm = rat::mdl::ModelMlfmm::create(model);
 //    mlfmm->set_output_dir(output_dir);
 //    mlfmm->set_target_meshes();

    

	// // calculate grid
	// if(run_grid){
	// 	// Create a grid
	// 	mlfmm->set_target_grid();
	// }

	// // calculate harmonics
	// if(run_harmonics){	
	// 	// create harmonic coil calculation
	// 	rat::mdl::ShCalcHarmonicsPr harm = rat::mdl::CalcHarmonics::create(pth_axis, radius*2.0/3.0);

	// 	// number of points used for harmonic calculation
	// 	harm->set_num_theta(64);
		
	// 	// curvature compensation
	// 	harm->set_compensate_curvature(true);

	// 	mlfmm->add_target(harm);
	// }

	// // run tracking code
	// if(run_tracking){
	// 	rat::mdl::ShCalcMeshPr tracking_mesh;
	// 	// set time
	// 	const rat::fltp track_time = 0;

	// 	// add the tracking mesh
	// 	mlfmm->add_target(tracking_mesh);

	// 	// Create a grid of 140x160x50 points in the volume defined by intervals:
	// 	rat::mdl::ShModelMeshPr beam_pipe = rat::mdl::ModelMesh::create(
	// 		pth_axis, rat::mdl::CrossCircle::create(radius,element_size));

	// 	// add an emitter
	// 	rat::mdl::ShEmitterBeamPr beam = rat::mdl::EmitterBeam::create();
	// 	beam->set_proton();
	// 	beam->set_beam_energy(beam->get_rest_mass() + 0.00405); // GeV
	// 	beam->set_spawn_coord(
	// 		arma::Col<double>::fixed<3>{0,0,0},
	// 		arma::Col<double>::fixed<3>{0,0,1},
	// 		arma::Col<double>::fixed<3>{1,0,0},
	// 		arma::Col<double>::fixed<3>{0,1,0});
	// 	beam->set_xx(0.9, 0.005, 0.0*2*arma::datum::pi/360);
	// 	beam->set_yy(0.9, 0.005, 0.0*2*arma::datum::pi/360);
	// 	beam->set_start_idx(0);

	// 	// create tracker
	// 	// create tracker
	// 	rat::mdl::ShCalcTracksPr tracker = rat::mdl::CalcTracks::create(tracking_mesh, beam);
	// 	tracker->set_stepsize(10e-3);

	// 	// perform tracking algorithm
	// 	tracker->create_tracks(lg);

	// 	// write to vtk
	// 	tracker->export_vtk()->write(output_dir/"tracks.vtu");
	// 	tracking_mesh->export_vtk()->write(output_dir/"tracking_mesh.vtu");
	// }

	// // run mlfmm
	// mlfmm->calculate(lg);

	// // run tracking code
	// if(run_tracking){

	// }





















}