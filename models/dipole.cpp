/* Rat - A heavilly vectorised and parallelised 
implementation of the Multi-Level Fast Multipole Method (MLFMM).
Copyright (C) 2017  Jeroen van Nugteren

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.*/

// general header files
#include <armadillo>
#include <iostream>
#include <cmath>
#include <complex>
#include <cassert>

// header files for common
#include "rat/common/gmshfile.hh"
#include "rat/common/log.hh"
#include "rat/common/opera.hh"
#include "rat/common/freecad.hh"

// header files for MLFMM
#include "rat/mlfmm/mlfmm.hh"
#include "rat/mlfmm/settings.hh"
#include "rat/mlfmm/background.hh"

// material header files
#include "rat/mat/parallelconductor.hh"
#include "rat/mat/rutherfordcable.hh"
#include "rat/mat/ltswire.hh"

// header files for Models
#include "rat/models/crossrectangle.hh"
#include "rat/models/modelcoil.hh"
#include "rat/models/pathcct.hh"
#include "rat/models/modelgroup.hh"
#include "rat/models/calcline.hh"
#include "rat/models/calcharmonics.hh"
#include "rat/models/crosscircle.hh"
#include "rat/models/pathaxis.hh"
#include "rat/models/calcgrid.hh"
#include "rat/models/transbend.hh"
#include "rat/models/serializer.hh"
#include "rat/models/modelmlfmm.hh"
#include "rat/models/modelinductance.hh"

// DESCRIPTION
// This example shows how to create a small CCT magnet with two layers.
// We use the default CCT magnet input which only supports one harmonic.
// For more customizable CCT geometries have a look at the custom_cct example.

// main
int main(){
	// INPUT SETTINGS
	// calculation settings
	const bool run_coil_field = true; // calculate field on mesh of coil
	const bool run_surface_field = true; // calculate field on surface of coil
	const bool run_harmonics = true; // calculate harmonics along aperture
	const bool run_grid = false; // calculate field on a volume grid
	const bool run_line = false; // calculate field on a volume grid
	const bool run_inductance = false; // calculate field on a volume grid

	// data storage
	const boost::filesystem::path output_dir = "./dipole/";

	// geometry settings
	const arma::uword num_poles = 1; // harmonic 1: dipole, 2: quadrupole, 3: sextupole, 4: octupole etc
	const arma::uword num_layers = 4; // number of layers in the coil 

	// coil geometry
	const bool is_reverse = false; // flag for reverse slanting
	const double bending_radius = 0.0; // ooil bending in [m] (0 for no bending)
	const double ell_coil = 0.4; // length of the coil in [m]
	//const double ell_coil = 2*arma::datum::pi*bending_radius/4; // coil length calculated from angle [m]
	const double aperture_radius = 30e-3; // aperture radius in [m]
	const double dradial = 0.3e-3; // spacing between cylinders [m]
	const double dformer = 2e-3; // thickness of formers [m]
	const arma::uword num_nodes_per_turn = 180;	// number of nodes each turn

	// geometry specific per layer
	const arma::Row<double> alpha = arma::Row<double>{20.0,26.0}*2*arma::datum::pi/360; // skew angle per two layers
	const arma::Row<arma::uword> is_horizontal = {0,0};

	// conductor geometryde
	const arma::uword nd = 2; // number of wires in width of slot
	const arma::uword nw = 4; // number of wires in depth of slot
	const double ddstr = 1.0e-3; // space allocated for insulated wire [m]
	const double dcable = nd*ddstr; // thickness of cable [m] (width of slot)
	const double wcable = nw*ddstr; // width of cable [m] (depth of slot)
	const double delta = 0.5e-3; // spacing between turns

	// operating current and temperature
	const arma::Row<double> operating_current = {420,420};
	const arma::Row<double> operating_temperature = {4.5,4.5};

	// background magnetic field
	arma::Col<double>::fixed<3> Hbg = {0,-1.0/arma::datum::mu_0,0};

	// GEOMETRY SETUP
	// filling fraction of conductors in the slot
	// const double ffill = nd*nw*arma::datum::pi*(dstr/2)*(dstr/2)/(dcable*wcable);

	// // create conductor
	// rat::mdl::ShMatGroupPr slot_conductor = rat::mdl::MatGroup::create();
	// {
	// 	// add nbti superconductor
	// 	rat::mdl::ShMatNbTiPr nbti = rat::mdl::MatNbTi::create();
	// 	nbti->set_nbti_LHC();
	// 	slot_conductor->add_material(ffill/(1.0+fcu2sc),nbti);

	// 	// add copper matrix
	// 	rat::mdl::ShMatCopperPr copper = rat::mdl::MatCopper::create();
	// 	copper->set_copper_OFHC_RRR100();
	// 	slot_conductor->add_material(ffill*fcu2sc/(1.0+fcu2sc),copper);
	// }

	// construct conductor
	rat::mat::ShLTSWirePr nbti_wire = 
		rat::mdl::Serializer::create<rat::mat::LTSWire>(
		"json/materials/ltswire/NbTi_LHC_1p9.json");
	rat::mat::ShRutherfordCablePr nbti_cable = 
		rat::mat::RutherfordCable::create();;
	nbti_cable->set_strand(nbti_wire);
	nbti_cable->set_num_strands(nd*nw);
	nbti_cable->set_width(wcable);
	nbti_cable->set_thickness(dcable);
	nbti_cable->set_keystone(0);
	nbti_cable->set_pitch(0);
	nbti_cable->set_dinsu(0);
	nbti_cable->set_fcabling(1.0);

	// create cable cross section
	// note that the cable is centered at least
	// in the thickness direction
	rat::mdl::ShCrossRectanglePr cross_rect = rat::mdl::CrossRectangle::create(
		-dcable/2,dcable/2,0,wcable,nd,nw);

	// create bending transformation
	rat::mdl::ShTransBendPr bending = rat::mdl::TransBend::create(0,1,0, 0,0,1, bending_radius);

	// create model that combines coils
	rat::mdl::ShModelGroupPr model = rat::mdl::ModelGroup::create();

	
	
	// walk over layers
	for(arma::uword i=0;i<num_layers;i++){
		// calculate radius
		const double coil_radius = aperture_radius + (i+1)*dformer + i*(wcable + dradial);
		const double inner_radius = aperture_radius + dformer + (i/2)*2*(dformer + wcable + dradial);

		// inner radius parameters
		const double omega = (delta + dcable)/std::sin(alpha(i/2));
		const double aest = inner_radius/(num_poles*std::tan(alpha(i/2))); // skew amplitude
		const double num_turns = 2*std::floor((ell_coil-2*aest)/(2*omega));

		// account for integer number of turns
		double a = num_poles*(ell_coil - num_turns*omega)/2;
		if(i%2==1)a+=omega/(num_poles*4); else a+=omega*(num_poles*4-1)/(num_poles*4);

		// create path
		rat::mdl::ShPathCCTPr path_cct = rat::mdl::PathCCT::create(
			num_poles,coil_radius,a,omega,num_turns,num_nodes_per_turn);
		
		// slanting direction
		if(i%2==0)path_cct->set_is_reverse(is_reverse); // reverse slanting
		else path_cct->set_is_reverse(!is_reverse); // reverse slanting
		
		// horizontal
		if(is_horizontal(i/2))path_cct->add_rotation(0,0,1,arma::datum::pi/2);

		// add bending
		path_cct->add_transformation(bending);

		// create inner and outer coils
		rat::mdl::ShModelCoilPr coil = rat::mdl::ModelCoil::create(path_cct, cross_rect, nbti_cable);
		coil->set_name("layer" + std::to_string(i)); 
		coil->set_number_turns(nd*nw);
		coil->set_operating_current(operating_current(i/2));
		coil->set_operating_temperature(operating_temperature(i/2));

		// add coil to model
		model->add_model(coil);
	}

	// axis of magnet for field quality calculation
	rat::mdl::ShPathAxisPr pth = rat::mdl::PathAxis::create('z','y',ell_coil+0.4,0,0,0,2e-3);
	pth->add_transformation(bending);



	// CALCULATION AND OUTPUT
	// create logger
	rat::cmn::ShLogPr lg = rat::cmn::Log::create(rat::cmn::Log::LogoType::RAT);

	// create calculation
	rat::mdl::ShModelMlfmmPr mlfmm = 
		rat::mdl::ModelMlfmm::create(model);
	mlfmm->set_output_dir(output_dir);
	mlfmm->set_target_meshes(run_coil_field); 
	mlfmm->set_target_grid(run_grid);
	mlfmm->set_target_surface(run_surface_field);

	// calculate field on line
	if(run_line){
		rat::mdl::ShCalcLinePr line = rat::mdl::CalcLine::create(pth);
		mlfmm->add_target(line);
	}

	// calculate everything
	mlfmm->calculate(lg);

	// inductance calculation
	if(run_inductance){
		// create inductance calculation
		rat::mdl::ShModelInductancePr ind = 
			rat::mdl::ModelInductance::create(model);

		// calculate and show matrix in terminal
		ind->calculate(lg); ind->display(lg);
	}

	// calculate harmonics
	if(run_harmonics){	
		// create calculation
		rat::mdl::ShModelMlfmmPr mlfmm_exp8 = rat::mdl::ModelMlfmm::create(model);
		mlfmm_exp8->set_output_dir(output_dir);

		// set number of expansions used for the spherical harmonics
		// higher is more accurate but also slower. The default is 5
		// but for harmonics we're using 8 here.
		mlfmm_exp8->get_settings()->set_num_exp(8);

		// create harmonic coil calculation along axis 
		// second input is the reference radius
		const rat::fltp reference_radius = 10e-3;
		rat::mdl::ShCalcHarmonicsPr harm = rat::mdl::CalcHarmonics::create(pth, reference_radius);
		harm->set_compensate_curvature(true);

		// number of points used for each 
		// harmonic calculation circle
		harm->set_num_theta(64);

		// add harmonics calculation as a target
		mlfmm_exp8->add_target(harm);

		// run calculation
		mlfmm_exp8->calculate(lg);

		// output to terminal
		harm->display(lg);
	}

}