## PROJECT SETUP
cmake_minimum_required(VERSION 3.9 FATAL_ERROR)
project(Rat-CCT VERSION 1.100.0 LANGUAGES CXX)

# set build type
if(NOT CMAKE_BUILD_TYPE)
  set(CMAKE_BUILD_TYPE Release)
endif()

# compiler options
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -Wextra")
set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} -g")
set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} -march=native -O3")
set(CMAKE_CXX_STANDARD 11)
set(CMAKE_POSITION_INDEPENDENT_CODE ON)

# find rat-models library
find_package(RatModels 1.100.0 REQUIRED)

# set the model
add_subdirectory(models)

# copy json database files to build directory
file(COPY json DESTINATION ${CMAKE_CURRENT_BINARY_DIR})

# install the binary
install(FILES ${CMAKE_CURRENT_BINARY_DIR}/models/jsoncct
	DESTINATION ${CMAKE_INSTALL_PREFIX}/bin
	PERMISSIONS WORLD_EXECUTE)
